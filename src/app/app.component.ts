import { Component, OnInit } from '@angular/core';
import { MapsService } from './maps.service';
import { interval, observable } from 'rxjs';
import { LatLng, LatLngLiteral, MapsAPILoader } from '@agm/core';
import { loadQueryList } from '@angular/core/src/render3';

declare const google;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // testLat: string = '';
  // testLng: string = '';

  vestLat: string = '';
  vestLng: string = '';
  fixedLat: string = '';
  fixedLng: string = '';
  gunLat: string = '';
  gunLng: string = '';

  testPolygon;
  testPolyline;
  showAlert = false;
  showLocationUpdate = false;
  message = '';

  delay: number = 6000; // six second interval

  location: object;

  source = interval(this.delay);

  paths: Array<LatLngLiteral> = [];

  constructor(private loader: MapsAPILoader, private map: MapsService) {}

  ngOnInit() {

    // mini function to make api calls every minute, but will convert to async listener
    // function to listen for any api changes
    // this.source.subscribe(() => {
    //   this.map.getLocation().subscribe(data => {
    //     console.log(data);
    //     this.vestLat = data.latitude;
    //     this.vestLng = data.longitude;
    //   });
    // });

    this.map.getLocation().subscribe(data => {
      console.log(data);
      // location where the vest/gun marker should be loaded,
      // but will change according to selected vest/gun ID/User
      this.vestLat = data.latitude;
      this.vestLng = data.longitude;
      // ------------------------------------------------------
      this.gunLat = data.latitude;
      this.gunLng = data.longitude;

      // location where the map should be loaded, currently determined by the vest location
      // but will change according to selected vest/gun location
      this.fixedLat = data.latitude;
      this.fixedLng = data.longitude;

      // points hardcoded to create "geofence" boundaries around vest location,
      // working on a simpler solution but it works for now [It's a square]
      this.createPolygon();

      this.testPolyline = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
      });
      // this.loader.add
      this.source.subscribe(() => {
        this.map.getLocation().subscribe(innerData => {
          // location where the vest/gun marker should be loaded,
          // but will change according to selected vest/gun ID/User
          this.vestLat = innerData.latitude;
          this.vestLng = innerData.longitude;

          this.gunLat = this.gunLat + 0.00003;
          this.gunLng = this.gunLng + 0.00003;

          const latLng = new google.maps.LatLng(this.gunLat, this.gunLng);
          this.showLocationUpdate = true;
          this.message = 'The gun\'s location has changed';
          // Check if the location is outside the polygon
          if (!google.maps.geometry.poly.containsLocation(latLng, this.testPolygon)) {
            // Show alert if user has left the polygon
            this.showAlert = true;
          } else {
            this.message = 'The user is currently in the ranch';
          }
          console.log(this.vestLat, this.vestLng);
        });
      });
    });
  }

  createPolygon() {
    // this.paths = [
    //   { lat: +this.gunLat + 0,  lng: +this.gunLng + 0.0002 }, // point 0
    //   { lat: +this.gunLat + 0.0002,  lng: +this.gunLng + 0.0002 }, // point 1
    //   { lat: +this.gunLat + 0.0002, lng: +this.gunLng + 0 }, // point 2
    //   { lat: +this.gunLat + 0.0002, lng: +this.gunLng - 0.0002 }, // point 3
    //   { lat: +this.gunLat + 0,  lng: +this.gunLng - 0.0002 }, // point 4
    //   { lat: +this.gunLat - 0.0002,  lng: +this.gunLng - 0.0002 }, // point 5
    //   { lat: +this.gunLat - 0.0002,  lng: +this.gunLng + 0 }, // point 6
    //   { lat: +this.gunLat - 0.0002, lng: +this.gunLng + 0.0002 }, // point 7
    //   { lat: +this.gunLat + 0,  lng: +this.gunLng + 0.0002 }, // point 8
    // ];
    this.paths = [
      { lat: +this.vestLat + 0,  lng: +this.vestLng + 0.0002 }, // point 0
      { lat: +this.vestLat + 0.0002,  lng: +this.vestLng + 0.0002 }, // point 1
      { lat: +this.vestLat + 0.0002, lng: +this.vestLng + 0 }, // point 2
      { lat: +this.vestLat + 0.0002, lng: +this.vestLng - 0.0002 }, // point 3
      { lat: +this.vestLat + 0,  lng: +this.vestLng - 0.0002 }, // point 4
      { lat: +this.vestLat - 0.0002,  lng: +this.vestLng - 0.0002 }, // point 5
      { lat: +this.vestLat - 0.0002,  lng: +this.vestLng + 0 }, // point 6
      { lat: +this.vestLat - 0.0002, lng: +this.vestLng + 0.0002 }, // point 7
      { lat: +this.vestLat + 0,  lng: +this.vestLng + 0.0002 }, // point 8
    ];
    this.testPolygon = new google.maps.Polygon({ paths: this.paths });
  }
}
