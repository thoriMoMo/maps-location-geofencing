import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Location {
  latitude: string;
  longitude: string;
}

@Injectable({
  providedIn: 'root'
})
export class MapsService {

  apiURL: string = 'https://ipapi.co/197.228.35.138/json/';

  constructor(private http: HttpClient) { }

  getLocation() {
    return this.http.get<Location>(this.apiURL);
  }
}
